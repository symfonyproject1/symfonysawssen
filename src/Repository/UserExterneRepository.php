<?php

namespace App\Repository;

use App\Entity\UserExterne;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserExterne|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserExterne|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserExterne[]    findAll()
 * @method UserExterne[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserExterneRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserExterne::class);
    }

    // /**
    //  * @return UserExterne[] Returns an array of UserExterne objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserExterne
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
