<?php

namespace App\Repository;

use App\Entity\Timming;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Timming|null find($id, $lockMode = null, $lockVersion = null)
 * @method Timming|null findOneBy(array $criteria, array $orderBy = null)
 * @method Timming[]    findAll()
 * @method Timming[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimmingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Timming::class);
    }

    // /**
    //  * @return Timming[] Returns an array of Timming objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Timming
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
