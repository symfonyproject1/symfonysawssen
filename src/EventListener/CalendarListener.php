<?php

namespace App\EventListener;

use App\Entity\Reservation;
use App\Repository\ReservationRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;

class CalendarListener
{
    private $reservationRepository;
    private $router;

    public function __construct(
        reservationRepository $reservationRepository,
        UrlGeneratorInterface $router
    ) {
        $this->reservationRepository = $reservationRepository;
        $this->router = $router;
    }

    public function load(CalendarEvent $calendar): void
    {
        $start = $calendar->getStart();
        $end = $calendar->getEnd();
        $filters = $calendar->getFilters();

        // Modify the query to fit to your entity and needs
        // Change booking.beginAt by your start date property
        $reservations = $this->reservationRepository
            ->createQueryBuilder('reservation')
            ->where('reservation.beginAt BETWEEN :start and :end')
            ->setParameter('start', $start->format('Y-m-d H:i:s'))
            ->setParameter('end', $end->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getResult()
        ;

        foreach ($reservations as $reservation) {
            // this create the events with your data (here booking data) to fill calendar
                $reservationEvent = new Event(
                $reservation->getTitle(),
                $reservation->getBeginAt(),
                $reservation->getEndAt() // If the end date is null or not defined, a all day event is created.
            );

            /*
             * Add custom options to events
             *
             * For more information see: https://fullcalendar.io/docs/event-object
             * and: https://github.com/fullcalendar/fullcalendar/blob/master/src/core/options.ts
             */

            $reservationEvent->setOptions([
                'backgroundColor' => "dechex(mt_rand(0,16777215))",
                'borderColor' => "#F56954",
            ]);
            $reservationEvent->addOption(
                'url',
                $this->router->generate('Reservation_show', [
                    'id' => $reservation->getId(),
                ])
            );

            // finally, add the event to the CalendarEvent to fill the calendar
            $calendar->addEvent($reservationEvent);
        }
    }
}
