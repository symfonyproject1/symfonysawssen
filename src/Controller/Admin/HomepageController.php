<?php
// src/AppBundle/Controller/AdminController.php
namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomepageController extends Controller
{
    
    /**
    * @Route("/admin", name="app_admin_homepage_index")
    **/
    public function showIndex()
    {
        
        return $this->render('admin/index.html.twig', ['mainNavMember'=>true, 'title'=>'Gestion de reservation: Espace Administrateur']);
    
    }   
}

