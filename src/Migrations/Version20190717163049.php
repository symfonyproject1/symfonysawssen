<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190717163049 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE etat (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_user (id INT AUTO_INCREMENT NOT NULL, typeuser VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE reservation ADD id_prix_id INT DEFAULT NULL, ADD id_timming_id INT DEFAULT NULL, ADD id_user_ex_id INT DEFAULT NULL, ADD id_etat_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C849554ADA8B82 FOREIGN KEY (id_prix_id) REFERENCES prix (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955DEE526D5 FOREIGN KEY (id_timming_id) REFERENCES timming (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955E733E0FE FOREIGN KEY (id_user_ex_id) REFERENCES user_externe (id)');
        $this->addSql('ALTER TABLE reservation ADD CONSTRAINT FK_42C84955D3C32F8F FOREIGN KEY (id_etat_id) REFERENCES etat (id)');
        $this->addSql('CREATE INDEX IDX_42C849554ADA8B82 ON reservation (id_prix_id)');
        $this->addSql('CREATE INDEX IDX_42C84955DEE526D5 ON reservation (id_timming_id)');
        $this->addSql('CREATE INDEX IDX_42C84955E733E0FE ON reservation (id_user_ex_id)');
        $this->addSql('CREATE INDEX IDX_42C84955D3C32F8F ON reservation (id_etat_id)');
        $this->addSql('ALTER TABLE user_externe ADD id_type_user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user_externe ADD CONSTRAINT FK_6AE70364AC8EC912 FOREIGN KEY (id_type_user_id) REFERENCES type_user (id)');
        $this->addSql('CREATE INDEX IDX_6AE70364AC8EC912 ON user_externe (id_type_user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C84955D3C32F8F');
        $this->addSql('ALTER TABLE user_externe DROP FOREIGN KEY FK_6AE70364AC8EC912');
        $this->addSql('DROP TABLE etat');
        $this->addSql('DROP TABLE type_user');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C849554ADA8B82');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C84955DEE526D5');
        $this->addSql('ALTER TABLE reservation DROP FOREIGN KEY FK_42C84955E733E0FE');
        $this->addSql('DROP INDEX IDX_42C849554ADA8B82 ON reservation');
        $this->addSql('DROP INDEX IDX_42C84955DEE526D5 ON reservation');
        $this->addSql('DROP INDEX IDX_42C84955E733E0FE ON reservation');
        $this->addSql('DROP INDEX IDX_42C84955D3C32F8F ON reservation');
        $this->addSql('ALTER TABLE reservation DROP id_prix_id, DROP id_timming_id, DROP id_user_ex_id, DROP id_etat_id');
        $this->addSql('DROP INDEX IDX_6AE70364AC8EC912 ON user_externe');
        $this->addSql('ALTER TABLE user_externe DROP id_type_user_id');
    }
}
