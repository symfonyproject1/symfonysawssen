<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190717145209 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE type_user DROP FOREIGN KEY FK_5A9C1341C54C8C93');
        $this->addSql('DROP TABLE etat');
        $this->addSql('DROP TABLE prix');
        $this->addSql('DROP TABLE timming');
        $this->addSql('DROP TABLE type_user');
        $this->addSql('DROP TABLE user_externe');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE etat (id INT AUTO_INCREMENT NOT NULL, etat_reser_id INT DEFAULT NULL, etat VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, INDEX IDX_55CAF7624B5E7131 (etat_reser_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE prix (id INT AUTO_INCREMENT NOT NULL, prix_reserv_id INT DEFAULT NULL, prix INT DEFAULT NULL, INDEX IDX_F7EFEA5ECFD87AB9 (prix_reserv_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE timming (id INT AUTO_INCREMENT NOT NULL, timming_reser_id INT DEFAULT NULL, timming VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, INDEX IDX_6DF0AB8B3B78DFD4 (timming_reser_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE type_user (id INT AUTO_INCREMENT NOT NULL, type_id INT DEFAULT NULL, type_user VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, INDEX IDX_5A9C1341C54C8C93 (type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE user_externe (id INT AUTO_INCREMENT NOT NULL, user_ex_id INT DEFAULT NULL, raison_social VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, objet VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, description LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, email VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, numero_de_tel INT NOT NULL, INDEX IDX_6AE7036442B96029 (user_ex_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE etat ADD CONSTRAINT FK_55CAF7624B5E7131 FOREIGN KEY (etat_reser_id) REFERENCES reservation (id)');
        $this->addSql('ALTER TABLE prix ADD CONSTRAINT FK_F7EFEA5ECFD87AB9 FOREIGN KEY (prix_reserv_id) REFERENCES reservation (id)');
        $this->addSql('ALTER TABLE timming ADD CONSTRAINT FK_6DF0AB8B3B78DFD4 FOREIGN KEY (timming_reser_id) REFERENCES reservation (id)');
        $this->addSql('ALTER TABLE type_user ADD CONSTRAINT FK_5A9C1341C54C8C93 FOREIGN KEY (type_id) REFERENCES user_externe (id)');
        $this->addSql('ALTER TABLE user_externe ADD CONSTRAINT FK_6AE7036442B96029 FOREIGN KEY (user_ex_id) REFERENCES reservation (id)');
    }
}
