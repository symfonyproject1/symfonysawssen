<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PrixRepository")
 */
class Prix
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $prix;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation", mappedBy="idPrix")
     */
    private $idReserv;

    public function __construct()
    {
        $this->idReserv = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getIdReserv(): Collection
    {
        return $this->idReserv;
    }

    public function addIdReserv(Reservation $idReserv): self
    {
        if (!$this->idReserv->contains($idReserv)) {
            $this->idReserv[] = $idReserv;
            $idReserv->setIdPrix($this);
        }

        return $this;
    }

    public function removeIdReserv(Reservation $idReserv): self
    {
        if ($this->idReserv->contains($idReserv)) {
            $this->idReserv->removeElement($idReserv);
            // set the owning side to null (unless already changed)
            if ($idReserv->getIdPrix() === $this) {
                $idReserv->setIdPrix(null);
            }
        }

        return $this;
    }
}
