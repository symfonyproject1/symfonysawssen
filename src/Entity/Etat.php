<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EtatRepository")
 */
class Etat
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation", mappedBy="idEtat")
     */
    private $idReserv;

    public function __construct()
    {
        $this->idReserv = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getIdReserv(): Collection
    {
        return $this->idReserv;
    }

    public function addIdReserv(Reservation $idReserv): self
    {
        if (!$this->idReserv->contains($idReserv)) {
            $this->idReserv[] = $idReserv;
            $idReserv->setIdEtat($this);
        }

        return $this;
    }

    public function removeIdReserv(Reservation $idReserv): self
    {
        if ($this->idReserv->contains($idReserv)) {
            $this->idReserv->removeElement($idReserv);
            // set the owning side to null (unless already changed)
            if ($idReserv->getIdEtat() === $this) {
                $idReserv->setIdEtat(null);
            }
        }

        return $this;
    }
}
