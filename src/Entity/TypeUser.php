<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeUserRepository")
 */
class TypeUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeuser;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserExterne", mappedBy="idTypeUser")
     */
    private $idUserEx;

    public function __construct()
    {
        $this->idUserEx = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeuser(): ?string
    {
        return $this->typeuser;
    }

    public function setTypeuser(string $typeuser): self
    {
        $this->typeuser = $typeuser;

        return $this;
    }

    /**
     * @return Collection|UserExterne[]
     */
    public function getIdUserEx(): Collection
    {
        return $this->idUserEx;
    }

    public function addIdUserEx(UserExterne $idUserEx): self
    {
        if (!$this->idUserEx->contains($idUserEx)) {
            $this->idUserEx[] = $idUserEx;
            $idUserEx->setIdTypeUser($this);
        }

        return $this;
    }

    public function removeIdUserEx(UserExterne $idUserEx): self
    {
        if ($this->idUserEx->contains($idUserEx)) {
            $this->idUserEx->removeElement($idUserEx);
            // set the owning side to null (unless already changed)
            if ($idUserEx->getIdTypeUser() === $this) {
                $idUserEx->setIdTypeUser(null);
            }
        }

        return $this;
    }
}
