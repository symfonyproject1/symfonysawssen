<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;


/**
 * Reservation
 *
 * @ORM\Table(name="reservation")
 * @ORM\Entity(repositoryClass="App\Repository\ReservationRepository")
 */
class Reservation
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @ORM\Id
     */
    private $id;
    /**
     * @ORM\Column(type="text")
     */
    private $objet;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

   /**
     * @var \Datetime
     *
     * @Assert\Type(
     *      type = "\DateTime",
     *      
     *     
     * )
     * @Assert\GreaterThanOrEqual(
     *      value = "now",
     *      message = "impossible de selectionner une date passé"
     * )
     * @ORM\Column(name="begin_at", type="datetime")
     */
    private $beginAt;
    
    /**
     * @var \Datetime
     * @Assert\Type(
     *      type = "\DateTime",
     *      
     *     
     * )
     * $beginAt->format('Y-m-d H:i:s');
     * @Assert\Expression("value > this.beginAt")
     * 
     * 
     * @ORM\Column(name="end_at", type="datetime")
     */
   
    private $endAt = null;

    /**
     * @ORM\Column(type="string", length=190)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="Reunion")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="idReunion", referencedColumnName="id")
     * })
     */
    private $idReunion;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="idUser", referencedColumnName="id")
     * })
     */
    private $iduser;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Prix", inversedBy="idReserv")
     */
    private $idPrix;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Timming", inversedBy="idReserv")
     */
    private $idTimming;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserExterne", inversedBy="idReserv")
     */
    private $idUserEx;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Etat", inversedBy="idReserv")
     */
    private $idEtat;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdUser(): ?int
    {
        return $this->iduser;
    }

    public function getIdSalle(): ?int
    {
        return $this->idsalle;
    }

    public function getBeginAt(): ?\DateTimeInterface
    {
        return $this->beginAt;
    }

    public function setBeginAt(\DateTimeInterface $beginAt): self
    {
        $this->beginAt = $beginAt;

        return $this;
    }

    public function getEndAt(): ?\DateTimeInterface
    {
        return $this->endAt;
    }

    public function setEndAt(?\DateTimeInterface $endAt = null): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getIdPrix(): ?Prix
    {
        return $this->idPrix;
    }

    public function setIdPrix(?Prix $idPrix): self
    {
        $this->idPrix = $idPrix;

        return $this;
    }

    public function getIdTimming(): ?Timming
    {
        return $this->idTimming;
    }

    public function setIdTimming(?Timming $idTimming): self
    {
        $this->idTimming = $idTimming;

        return $this;
    }

    public function getIdUserEx(): ?UserExterne
    {
        return $this->idUserEx;
    }

    public function setIdUserEx(?UserExterne $idUserEx): self
    {
        $this->idUserEx = $idUserEx;

        return $this;
    }

    public function getIdEtat(): ?Etat
    {
        return $this->idEtat;
    }

    public function setIdEtat(?Etat $idEtat): self
    {
        $this->idEtat = $idEtat;

        return $this;
    }
    public function reverseTransform($beginAt)
    {
        // datetime optional
        if (!$beginAt) {
            return;
        }

        return date_create_from_format('d/m/Y H:i', $beginAt, new \DateTimeZone('Europe/Madrid'));
       if (!$endAt) {
            return;
        }

        return date_create_from_format('d/m/Y H:i', $endAt, new \DateTimeZone('Europe/Madrid'));
    }
  
}


