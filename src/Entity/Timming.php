<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TimmingRepository")
 */
class Timming
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $timming;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $time;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation", mappedBy="idTimming")
     */
    private $idReserv;

    public function __construct()
    {
        $this->idReserv = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimming(): ?string
    {
        return $this->timming;
    }

    public function setTimming(string $timming): self
    {
        $this->timming = $timming;

        return $this;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(string $time): self
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getIdReserv(): Collection
    {
        return $this->idReserv;
    }

    public function addIdReserv(Reservation $idReserv): self
    {
        if (!$this->idReserv->contains($idReserv)) {
            $this->idReserv[] = $idReserv;
            $idReserv->setIdTimming($this);
        }

        return $this;
    }

    public function removeIdReserv(Reservation $idReserv): self
    {
        if ($this->idReserv->contains($idReserv)) {
            $this->idReserv->removeElement($idReserv);
            // set the owning side to null (unless already changed)
            if ($idReserv->getIdTimming() === $this) {
                $idReserv->setIdTimming(null);
            }
        }

        return $this;
    }
}
