<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserExterneRepository")
 */
class UserExterne
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $raisonSocial;

   

    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="integer")
     */
    private $numeroTel;

    /**
     * @ORM\Column(type="integer")
     */
    private $numTel;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation", mappedBy="idUserEx")
     */
    private $idReserv;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeUser", inversedBy="idUserEx")
     */
    private $idTypeUser;

    public function __construct()
    {
        $this->idReserv = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRaisonSocial(): ?string
    {
        return $this->raisonSocial;
    }

    public function setRaisonSocial(string $raisonSocial): self
    {
        $this->raisonSocial = $raisonSocial;

        return $this;
    }

    public function getObjet(): ?string
    {
        return $this->objet;
    }

    public function setObjet(string $objet): self
    {
        $this->objet = $objet;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getNumeroTel(): ?int
    {
        return $this->numeroTel;
    }

    public function setNumeroTel(int $numeroTel): self
    {
        $this->numeroTel = $numeroTel;

        return $this;
    }

    public function getNumTel(): ?int
    {
        return $this->numTel;
    }

    public function setNumTel(int $numTel): self
    {
        $this->numTel = $numTel;

        return $this;
    }

    /**
     * @return Collection|Reservation[]
     */
    public function getIdReserv(): Collection
    {
        return $this->idReserv;
    }

    public function addIdReserv(Reservation $idReserv): self
    {
        if (!$this->idReserv->contains($idReserv)) {
            $this->idReserv[] = $idReserv;
            $idReserv->setIdUserEx($this);
        }

        return $this;
    }

    public function removeIdReserv(Reservation $idReserv): self
    {
        if ($this->idReserv->contains($idReserv)) {
            $this->idReserv->removeElement($idReserv);
            // set the owning side to null (unless already changed)
            if ($idReserv->getIdUserEx() === $this) {
                $idReserv->setIdUserEx(null);
            }
        }

        return $this;
    }

    public function getIdTypeUser(): ?TypeUser
    {
        return $this->idTypeUser;
    }

    public function setIdTypeUser(?TypeUser $idTypeUser): self
    {
        $this->idTypeUser = $idTypeUser;

        return $this;
    }
}
